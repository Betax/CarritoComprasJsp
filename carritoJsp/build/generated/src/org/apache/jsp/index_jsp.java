package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Modelo.*;
import java.util.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\r\n");
      out.write("\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("<head>\r\n");
      out.write("\t<meta charset=\"utf-8\" />\r\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />\r\n");
      out.write("\t<title></title>\r\n");
      out.write("\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\" />\r\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\" />\r\n");
      out.write("\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>\r\n");
      out.write("\t\r\n");
      out.write("\t<script src=\"js/jquery-1.8.0.min.js\" type=\"text/javascript\"></script>\r\n");
      out.write("\t<!--[if lt IE 9]>\r\n");
      out.write("\t\t<script src=\"js/modernizr.custom.js\"></script>\r\n");
      out.write("\t<![endif]-->\r\n");
      out.write("\t<script src=\"js/jquery.carouFredSel-5.5.0-packed.js\" type=\"text/javascript\"></script>\r\n");
      out.write("\t<script src=\"js/functions.js\" type=\"text/javascript\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("<!-- wrapper -->\r\n");
      out.write("<div id=\"wrapper\">\r\n");
      out.write("\t<!-- shell -->\r\n");
      out.write("\t<div class=\"shell\">\r\n");
      out.write("\t\t<!-- container -->\r\n");
      out.write("\t\t<div class=\"container\">\r\n");
      out.write("\t\t\t<!-- header -->\r\n");
      out.write("\t\t\t<header id=\"header\">\r\n");
      out.write("\t\t\t\t<h1 id=\"logo\">CARRITO DE COMPRAS</h1>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\r\n");
      out.write("\t\t  </header>\r\n");
      out.write("\t\t\t<!-- end of header -->\r\n");
      out.write("\t\t\t<!-- navigaation -->\r\n");
      out.write("<nav id=\"navigation\">\r\n");
      out.write("\t\t\t\t<a href=\"#\" class=\"nav-btn\">HOME<span></span></a>\r\n");
      out.write("\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t<li class=\"active\"><a href=\"index.jsp\">Catalogo</a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"registrarProducto.jsp\">Registrar Producto</a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"registrarVenta.jsp\">Registrar Ventas</a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"consultarVentas.jsp\">Consultar Ventas</a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"\">Logueo de usuarios</a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\r\n");
      out.write("\t\t\t</nav>\r\n");
      out.write("  <!-- Inicio de JSP --> \r\n");
      out.write("  \r\n");
      out.write("                    <h2 align=\"center\">Lista de Productos</h2>\r\n");
      out.write("               \r\n");
      out.write("            <table border=\"0\" align=\"center\" width=\"100%\">\r\n");
      out.write("                \r\n");
      out.write("                \r\n");
      out.write("                ");
      out.write("\r\n");
      out.write("                ");

                  ArrayList<Producto> lista = ProductoBD.obtenerProducto();
                  int salto=0;
                  for (Producto p : lista) {
                
      out.write("\r\n");
      out.write("                \r\n");
      out.write("                    <th><img src=\"imagenes/");
      out.print(p.getImagen());
      out.write("\" width=\"140\" height=\"140\"><p>\r\n");
      out.write("                    ");
      out.print( p.getNombre());
      out.write("<br>\r\n");
      out.write("                    ");
      out.print( p.getPrecio());
      out.write("<p>\r\n");
      out.write("                    ");
      out.write("\r\n");
      out.write("                    <a href=\"actualizarProducto.jsp?id=");
      out.print( p.getCodigoProducto());
      out.write("\">Modificar</a> |\r\n");
      out.write("                        <a href=\"anadirCarrito.jsp?id=");
      out.print( p.getCodigoProducto());
      out.write("\">Añadir</a>\r\n");
      out.write("                    </th>\r\n");
      out.write("                \r\n");
      out.write("                ");

                salto++;
                if(salto==3){
                    
      out.write("\r\n");
      out.write("                    <tr> \r\n");
      out.write("                        ");

                        salto=0;
                }
                            }
                
      out.write("\r\n");
      out.write("\r\n");
      out.write("            </table>\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write(" <!-- final de js -->\r\n");
      out.write(" \r\n");
      out.write("\t\t\t  </section>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<!-- end of main -->\r\n");
      out.write("\t\t\t<div class=\"cl\">&nbsp;</div>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<!-- footer -->\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<!-- end of footer -->\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<!-- end of container -->\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<!-- end of shell -->\r\n");
      out.write("</div>\r\n");
      out.write("<!-- end of wrapper -->\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
