package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class registrarProducto_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("\t<meta charset=\"utf-8\" />\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />\n");
      out.write("\t<title>Carrito de Compras JSP - Eduardo Reyes Rodriguez</title>\n");
      out.write("\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\" />\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\" />\n");
      out.write("\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>\n");
      out.write("\t\n");
      out.write("\t<script src=\"js/jquery-1.8.0.min.js\" type=\"text/javascript\"></script>\n");
      out.write("\t<!--[if lt IE 9]>\n");
      out.write("\t\t<script src=\"js/modernizr.custom.js\"></script>\n");
      out.write("\t<![endif]-->\n");
      out.write("\t<script src=\"js/jquery.carouFredSel-5.5.0-packed.js\" type=\"text/javascript\"></script>\n");
      out.write("\t<script src=\"js/functions.js\" type=\"text/javascript\"></script>\n");
      out.write("        <style>\n");
      out.write("            #centro{\n");
      out.write("    width: 300px;\n");
      out.write("    margin: 0 auto;\n");
      out.write("    height: 250px;\n");
      out.write("    \n");
      out.write("}\n");
      out.write("            </style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<!-- wrapper -->\n");
      out.write("<div id=\"wrapper\">\n");
      out.write("\t<!-- shell -->\n");
      out.write("\t<div class=\"shell\">\n");
      out.write("\t\t<!-- container -->\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<!-- header -->\n");
      out.write("\t\t\t<header id=\"header\">\n");
      out.write("\t\t\t\t<h1 id=\"logo\">CARRITO DE COMPRAS</h1>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t  </header>\n");
      out.write("\t\t\t<!-- end of header -->\n");
      out.write("\t\t\t<!-- navigaation -->\n");
      out.write("<nav id=\"navigation\">\n");
      out.write("\t\t\t\t<a href=\"#\" class=\"nav-btn\">HOME<span></span></a>\n");
      out.write("\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t<li class=\"active\"><a href=\"index.jsp\">Catalogo</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"registrarProducto.jsp\">Registrar Producto</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"\">Registrar Ventas</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"\">Consultar Ventas</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"\">Logueo de usuarios</a></li>\n");
      out.write("                                </ul>\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t</nav>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- main -->\n");
      out.write("\t\t  <div class=\"main\">\n");
      out.write("\n");
      out.write("\t\t\t  <div class=\"featured\">\n");
      out.write("\t\t\t\t<h4><strong>REGISTRO DE PRODUCTOS</strong> </h4>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t  <div id=\"centro\">\n");
      out.write("          ");
      out.write("    \n");
      out.write("              \n");
      out.write("        ");
      out.write("\n");
      out.write("        <h1 align=\"center\">Nuevos Productos</h1>\n");
      out.write("        <form method=\"post\" action=\"ServletControlador\">\n");
      out.write("            <div id=\"porfavor\">\n");
      out.write("                <input type=\"hidden\" name=\"accion\" value=\"RegistrarProducto\" />\n");
      out.write("                <table border=\"0\" width=\"300\" align=\"center\" id=\"tabla1\">\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Nombre</td>\n");
      out.write("                        <td><input type=\"text\" name=\"txtNombre\" value=\"\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Precio</td>\n");
      out.write("                        <td><input type=\"text\" name=\"txtPrecio\" value=\"0\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td>Imagen</td>\n");
      out.write("                        <td><input type=\"text\" name=\"txtImagen\" value=\"0\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                    <tr>\n");
      out.write("                        <td></td>\n");
      out.write("                        <td><input type=\"submit\" value=\"Registrar\" name=\"btnRegistrar\" /></td>\n");
      out.write("                    </tr>\n");
      out.write("                </table>\n");
      out.write("            </div>\n");
      out.write("        </form>\n");
      out.write("\n");
      out.write("    ");
      out.write("           \n");
      out.write("      \n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
