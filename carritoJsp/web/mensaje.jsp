<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
	<title></title>
	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
	
	<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
		<script src="js/modernizr.custom.js"></script>
	<![endif]-->
	<script src="js/jquery.carouFredSel-5.5.0-packed.js" type="text/javascript"></script>
	<script src="js/functions.js" type="text/javascript"></script>
        <style>
            #centro{
    width: 300px;
    margin: 0 auto;
    height: 400px;
    
}
            </style>
</head>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- shell -->
	<div class="shell">
		<!-- container -->
		<div class="container">
			<!-- header -->
			<header id="header">
				<h1 id="logo">CARRITO DE COMPRAS</h1>
				
				<div class="cl">&nbsp;</div>
		  </header>
			<!-- end of header -->
			<!-- navigaation -->
<nav id="navigation">
				<a href="#" class="nav-btn">HOME<span></span></a>
				<ul>
					<li class="active"><a href="index.jsp">Catalogo</a></li>
					<li><a href="registrarProducto.jsp">Registrar Producto</a></li>
					<li><a href="registrarVenta.jsp">Registrar Ventas</a></li>
					<li><a href="consultarVentas.jsp">Consultar Ventas</a></li>
					<li><a href="">Logueo de usuarios</a></li>
                                </ul>
				<div class="cl">&nbsp;</div>
			</nav>

			<!-- main -->
		  <div class="main">

			  <div class="featured">
				<h4> <strong>MENSAJE DE CONFIRMACION</strong> </h4>
				</div>
			  <div id="centro">
        <h3 align="center">
            <%
            if(request.getParameter("men")!=null){
                out.println(request.getParameter("men"));
            }
            
            %>
            <br>
            <a href="index.jsp">Volver a Principal</a>
        </h3>
    </div>
			<!-- end of main -->
			<div class="cl">&nbsp;</div>
			
			<!-- footer -->
			
			<!-- end of footer -->
		</div>
		<!-- end of container -->
	</div>
	<!-- end of shell -->
</div>
<!-- end of wrapper -->
</body>
</html>