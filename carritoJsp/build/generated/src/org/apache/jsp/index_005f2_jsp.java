package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Modelo.*;
import java.util.*;

public final class index_005f2_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n");
      out.write("    <title>JSP Page</title>\r\n");
      out.write("    </head>\r\n");
      out.write("   <body background=\"espacio1.jpg\">\r\n");
      out.write("           <h2 align=\"center\">Carrito de Compras</h2>\r\n");
      out.write("           <h3 align=\"center\">\r\n");
      out.write("            ");
      out.write("\r\n");
      out.write("            <a href=\"index.jsp\">Inicio</a> |\r\n");
      out.write("            <a href=\"registrarProducto.jsp\">Registrar Producto</a> |\r\n");
      out.write("            <a href=\"registrarVenta.jsp\">Registrar Venta</a> |\r\n");
      out.write("            <a href=\"consultarVentas.jsp\">Consultar Ventas</a> |\r\n");
      out.write("           </h3>\r\n");
      out.write("        <br >\r\n");
      out.write("        <div>\r\n");
      out.write("   \r\n");
      out.write("                    <h2 align=\"center\">Lista de Productos</h2>\r\n");
      out.write("               \r\n");
      out.write("            <table border=\"0\" align=\"center\" width=\"700\">\r\n");
      out.write("                \r\n");
      out.write("                \r\n");
      out.write("                ");
      out.write("\r\n");
      out.write("                ");

                  ArrayList<Producto> lista = ProductoBD.obtenerProducto();
                  int salto=0;
                  for (Producto p : lista) {
                
      out.write("\r\n");
      out.write("                \r\n");
      out.write("                    <th><img src=\"imagenes/");
      out.print(p.getImagen());
      out.write(".jpg\" width=\"95\" height=\"95\"><p>\r\n");
      out.write("                    ");
      out.print( p.getNombre());
      out.write("<br>\r\n");
      out.write("                    ");
      out.print( p.getPrecio());
      out.write("<p>\r\n");
      out.write("                    ");
      out.write("\r\n");
      out.write("                    <a href=\"actualizarProducto.jsp?id=");
      out.print( p.getCodigoProducto());
      out.write("\">Modificar</a> |\r\n");
      out.write("                        <a href=\"anadirCarrito.jsp?id=");
      out.print( p.getCodigoProducto());
      out.write("\">Añadir</a>\r\n");
      out.write("                    </th>\r\n");
      out.write("                \r\n");
      out.write("                ");

                salto++;
                if(salto==3){
                    
      out.write("\r\n");
      out.write("                    <tr> \r\n");
      out.write("                        ");

                        salto=0;
                }
                            }
                
      out.write("\r\n");
      out.write("\r\n");
      out.write("            </table>\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
