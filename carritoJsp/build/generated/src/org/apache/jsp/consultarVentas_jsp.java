package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Modelo.*;
import java.util.*;

public final class consultarVentas_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("\t<meta charset=\"utf-8\" />\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />\n");
      out.write("\t<title>Carrito de Compras JSP - Eduardo Reyes Rodriguez</title>\n");
      out.write("\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"css/images/favicon.ico\" />\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\" media=\"all\" />\n");
      out.write("\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>\n");
      out.write("\t\n");
      out.write("\t<script src=\"js/jquery-1.8.0.min.js\" type=\"text/javascript\"></script>\n");
      out.write("\t<!--[if lt IE 9]>\n");
      out.write("\t\t<script src=\"js/modernizr.custom.js\"></script>\n");
      out.write("\t<![endif]-->\n");
      out.write("\t<script src=\"js/jquery.carouFredSel-5.5.0-packed.js\" type=\"text/javascript\"></script>\n");
      out.write("\t<script src=\"js/functions.js\" type=\"text/javascript\"></script>\n");
      out.write("        <style>\n");
      out.write("            #centro{\n");
      out.write("    width: 300px;\n");
      out.write("    margin: 0 auto;\n");
      out.write("    height: 400px;\n");
      out.write("    \n");
      out.write("}\n");
      out.write("            </style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<!-- wrapper -->\n");
      out.write("<div id=\"wrapper\">\n");
      out.write("\t<!-- shell -->\n");
      out.write("\t<div class=\"shell\">\n");
      out.write("\t\t<!-- container -->\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<!-- header -->\n");
      out.write("\t\t\t<header id=\"header\">\n");
      out.write("\t\t\t\t<h1 id=\"logo\">CARRITO DE COMPRAS</h1>\n");
      out.write("\t\t\t\t<!-- search -->\n");
      out.write("\t\t\t\t<div class=\"search\">\n");
      out.write("\t\t\t\t\t<form action=\"\" method=\"post\">\n");
      out.write("\t\t\t\t\t\t<input type=\"text\" class=\"field\" value=\"keywords here ...\" title=\"keywords here ...\" />\n");
      out.write("\t\t\t\t\t\t<input type=\"submit\" class=\"search-btn\" value=\"\" />\n");
      out.write("\t\t\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t\t\t</form>\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t\t<!-- end of search -->\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t  </header>\n");
      out.write("\t\t\t<!-- end of header -->\n");
      out.write("\t\t\t<!-- navigaation -->\n");
      out.write("<nav id=\"navigation\">\n");
      out.write("\t\t\t\t<a href=\"#\" class=\"nav-btn\">HOME<span></span></a>\n");
      out.write("\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t<li class=\"active\"><a href=\"index.jsp\">Catalogo</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"registrarProducto.jsp\">Registrar Producto</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"registrarVenta.jsp\">Registrar Ventas</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"consultarVentas.jsp\">Consultar Ventas</a></li>\n");
      out.write("\t\t\t\t\t<li><a href=\"\">Logueo de usuarios</a></li>\n");
      out.write("                                </ul>\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t</nav>\n");
      out.write("\t\t\t<!-- end of navigation -->\n");
      out.write("\t\t\t<!-- slider-holder -->\n");
      out.write("\t\t\t<div class=\"slider-holder\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t<!-- slider -->\n");
      out.write("\t\t\t\t<div class=\"slider\">\n");
      out.write("\t\t\t\t\t<div class=\"socials\">\n");
      out.write("\t\t\t\t\t\t<a href=\"#\" class=\"facebook-ico\">facebook-ico</a>\n");
      out.write("\t\t\t\t\t\t<a href=\"#\" class=\"twitter-ico\">twitter-ico</a>\n");
      out.write("\t\t\t\t\t\t<a href=\"#\" class=\"skype-ico\">skype-ico</a>\n");
      out.write("\t\t\t\t\t\t<a href=\"#\" class=\"rss-ico\">rss-ico</a>\n");
      out.write("\t\t\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\n");
      out.write("\n");
      out.write("\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t<li id=\"img1\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"slide-cnt\">\n");
      out.write("\t\t\t\t\t\t\t\t<h4>Eduardo Reyes</h4>\n");
      out.write("\t\t\t\t\t\t\t\t<h2>Artefactos</h2>\n");
      out.write("\t\t\t\t\t\t\t\t<p>En nuestra cadena de tiendas podras encontrar los mejores productos a un bajo costo y lo mejor que lo podras pagar en comodas cuotas en el tiempo que desees.</a></p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<img src=\"css/images/artefactos1.png\" alt=\"\" />\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("                                                <li id=\"img2\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"slide-cnt\">\n");
      out.write("\t\t\t\t\t\t\t\t<h4>Eduardo Reyes</h4>\n");
      out.write("\t\t\t\t\t\t\t\t<h2>Computadoras</h2>\n");
      out.write("\t\t\t\t\t\t\t\t<p>Las mejores computadoras, las mejores marcas y con una garantia total, podras encontrar en nuestras tiendas, ademas te instalamos todos los software necesarios para que puedas trabajar</p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<img src=\"css/images/artefactos2.PNG\" alt=\"\" />\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("                                            \n");
      out.write("                        \n");
      out.write("                                                <li id=\"img3\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"slide-cnt\">\n");
      out.write("\t\t\t\t\t\t\t\t<h4>Eduardo Reyes</h4>\n");
      out.write("\t\t\t\t\t\t\t\t<h2>Accesorios</h2>\n");
      out.write("\t\t\t\t\t\t\t\t<p>En la tienda podras encontrar todos los accesorios que buscas para poder equipar tu casa y tu oficna desde accesorios de artefactos hasta accesorios de computo, con la misma garantia que tiene todos los productos de tiendas Eduardo Reyes</a></p>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<img src=\"css/images/artefactos3.png\" alt=\"\" />\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end of slider -->\n");
      out.write("\n");
      out.write("\t\t\t\t<!-- thumbs -->\n");
      out.write("\t\t\t\t<div id=\"thumbs-wrapper\">\n");
      out.write("\t\t\t\t\t<div id=\"thumbs\">\n");
      out.write("\t\t\t\t\t\t<a href=\"#img1\" class=\"selected\"><img src=\"css/images/artefactos1.png\"/></a>\n");
      out.write("\t\t\t\t\t\t<a href=\"#img2\"><img src=\"css/images/artefactos2.PNG\" /></a>\n");
      out.write("                        <a href=\"#img3\"><img src=\"css/images/artefactos3.png\" /></a>\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<a id=\"prev\" href=\"#\"></a>\n");
      out.write("\t\t\t\t\t<a id=\"next\" href=\"#\"></a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<!-- end of thumbs -->\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- main -->\n");
      out.write("\t\t  <div class=\"main\">\n");
      out.write("\n");
      out.write("\t\t\t  <div class=\"featured\">\n");
      out.write("\t\t\t\t<h4>Carrito JSP -  <strong>CONSULTA DE VENTAS</strong> </h4>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t  <div id=\"centro\">\n");
      out.write("                              <h2 align=\"center\">Consulta de Ventas</h2>\n");
      out.write("            <table border=\"1\" align=\"left\" width=\"400\">\n");
      out.write("                <tr style=\"background-color: skyblue; color: black; font-weight: bold\">\n");
      out.write("                    <th colspan=\"4\">Lista de Ventas</th>\n");
      out.write("                </tr>\n");
      out.write("                <tr style=\"background-color: skyblue; color: black; font-weight: bold\">\n");
      out.write("                    <th>Codigo</th><th>Nombre</th><th>Precio</th><th>Accion</th>\n");
      out.write("                </tr>\n");
      out.write("                ");
      out.write("\n");
      out.write("                ");

                            ArrayList<Venta> lista = VentaBD.obtenerVentas();
                                 for (Venta v : lista) {
                
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <td>");
      out.print( v.getCodigoVenta() );
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( v.getCliente() );
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( v.getFecha() );
      out.write("</td>\n");
      out.write("                    ");
      out.write("\n");
      out.write("                    <td><a href=\"?cod=");
      out.print( v.getCodigoVenta() );
      out.write("\">ver Detalle</a></td>\n");
      out.write("               \n");
      out.write("                    \n");
      out.write("                    </td>\n");
      out.write("                </tr>\n");
      out.write("                ");

                            }
                
      out.write("\n");
      out.write("\n");
      out.write("            </table>\n");
      out.write("       <p>\n");
      out.write("            <div id=\"mostrarDato\">\n");
      out.write("                <iframe name=\"detalle\" width=\"500\" height=\"400\" frameborder=\"0\">\n");
      out.write("                \n");
      out.write("                </iframe>\n");
      out.write("            </div>\n");
      out.write("    </div>\n");
      out.write("\t\t\t<!-- end of main -->\n");
      out.write("\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t<!-- footer -->\n");
      out.write("\t\t\t<div id=\"footer\">\n");
      out.write("\t\t\t\t<div class=\"footer-nav\">\n");
      out.write("\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">About</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Services</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Projects</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Solutions</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Jobs</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Blog</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Contacts</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<p class=\"copy\">&copy; Copyright 2012<span>|</span>Sitename. Design by <a href=\"http://chocotemplates.com\" target=\"_blank\">ChocoTemplates.com</a></p>\n");
      out.write("\t\t\t\t<div class=\"cl\">&nbsp;</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!-- end of footer -->\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!-- end of container -->\n");
      out.write("\t</div>\n");
      out.write("\t<!-- end of shell -->\n");
      out.write("</div>\n");
      out.write("<!-- end of wrapper -->\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
