<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Modelo.*" %>
<%@page import="java.util.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
	<title></title>
	<link rel="shortcut icon" type="image/x-icon" href="css/images/favicon.ico" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
	
	<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
		<script src="js/modernizr.custom.js"></script>
	<![endif]-->
	<script src="js/jquery.carouFredSel-5.5.0-packed.js" type="text/javascript"></script>
	<script src="js/functions.js" type="text/javascript"></script>
        <style>
            #centro{
    width: 300px;
    margin: 0 auto;
    height: 400px;
    
}
            </style>
</head>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- shell -->
	<div class="shell">
		<!-- container -->
		<div class="container">
			<!-- header -->
			<header id="header">
				<h1 id="logo">CARRITO DE COMPRAS</h1>
				
				<div class="cl">&nbsp;</div>
		  </header>
			<!-- end of header -->
			<!-- navigaation -->
<nav id="navigation">
				<a href="#" class="nav-btn">HOME<span></span></a>
				<ul>
					<li><a href="index.jsp">Catalogo</a></li>
					<li><a href="registrarProducto.jsp">Registrar Producto</a></li>
					<li class="active"><a href="registrarVenta.jsp">Registrar Ventas</a></li>
					<li><a href="consultarVentas.jsp">Consultar Ventas</a></li>
					<li><a href="">Logueo de usuarios</a></li>
                                </ul>
				<div class="cl">&nbsp;</div>
			</nav>
			<!-- main -->
		  <div class="main">

			  <div class="featured">
				<h4><strong>REGISTRO DE VENTAS</strong> </h4>
				</div>
			  <div id="centro">
          <%-- Inicio del jsp--%>    
              
          <h1 align="center">Carrito de Compras</h1><h2></h2>
<div>
<form method="post" action="ServletControlador">
<%-- Llamamos a la accion Registrar Venta --%>
<input type="hidden" name="accion" value="RegistrarVenta" />
<table border="1" align="center" width="400">
    <tr style="background-color: skyblue; color: black; font-weight: bold">
        <td colspan="5">Carrito de Compras</td>
    </tr>
    <tr style="background-color: skyblue; color: black; font-weight: bold">
        <td>Cliente:</td>
        <td colspan="4"><input type="text" name="txtCliente" value="" /></td>
    </tr>
    <tr style="background-color: skyblue; color: black; font-weight: bold">
        <td>Nombre</td>
        <td>Precio</td>
        <td>Cantidad</td>
        <td>Descuento</td>
        <td>Sub. Total</td>
    </tr>
    <%-- Los productos que tenemos en la sesion que se llama carrito --%>
    <%
    double total=0;
        ArrayList<DetalleVenta> lista = (ArrayList<DetalleVenta>)session.getAttribute("carrito");
        if(lista!=null){
            
            for (DetalleVenta d : lista) {
    %>
                    <tr>
                        <td><%= d.getProducto()%></td>
                        <td><%= d.getProducto().getPrecio()%></td>
                        <td><%= d.getCantidad()%></td>
                        <td><%= d.getDescuento()%></td>
                        <td><%= (d.getProducto().getPrecio() * d.getCantidad())-d.getDescuento()%></td>
                    </tr>
    <%
    
    
    total=total+(d.getProducto().getPrecio() * d.getCantidad())-d.getDescuento();
                    }
                }
    %>
    <tr>
       
        <th colspan="4" align="right">Total</th>
         <th><%=total%></th>
    </tr>
    <tr >
        <td colspan="5"><input type="submit" value="Registrar Venta" name="btnVenta" /></td>
    </tr>
</table>
</form>
</div>
    <h3 align="center"><a href="index.jsp">Seguir Comprando</a></h3>
  </div>
			</div>
			<!-- end of main -->
			<div class="cl">&nbsp;</div>
			
			<!-- footer -->
		
			<!-- end of footer -->
		</div>
		<!-- end of container -->
	</div>
	<!-- end of shell -->
</div>
<!-- end of wrapper -->
</body>
</html>