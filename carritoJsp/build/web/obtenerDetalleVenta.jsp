<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Modelo.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
            <table border="1" align="left" width="400">
                
                <tr style="background-color: skyblue; color: black; font-weight: bold">
                    <th>CodigoVenta</th><th>CodigoProducto</th>
                    <th>Cantidad</th><th>Descuento</th>
                </tr>
                <%-- Lista de todos los productos --%>
                <%
                            ArrayList<DetalleVenta> lista = DetalleVentaBD.obtenerDetalleVenta(Integer.parseInt(request.getParameter("cod")));
                                     
                                 for (int i=0;i<lista.size();i++) {
                                     DetalleVenta d=lista.get(i);
                %>
                <tr>
                    <td><%= d.getCodigoVenta() %></td>
                    <td><%= d.getCodigoProducto() %></td>
                    <td><%= d.getCantidad() %></td>
                    <td><%= d.getDescuento() %></td>
                </tr>
                <%
                            }
                %>
            </table>
            
    </body>
</html>
